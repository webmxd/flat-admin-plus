export default {
  /**
   * 页面标签的title
   */
  title: 'fiat-admin',
  /**
   * 侧边栏菜单顶部标题，为空则只显示logo
   */
  sidebarLogoTitle: '后台管理系统',
  /**
   * 布局设置
   */
  // 侧边栏的宽度，单位px，不可动态修改，需要和 styles/variables.less中 @base-sider-width 保持一致
  sideBarWidth: 250,
  // 侧边栏的收缩宽度，单位px，不可动态修改，需要和 styles/variables.less中 @base-sider-collaps-width 保持一致
  sideBarCollapseWidth: 64,
  layout: {
    // 侧边栏主题色：深色主题dark，浅色主题light，默认深色dark
    sideTheme: 'light',
    // 是否固定顶栏
    fixedHeader: true,
    // 是否固定左侧栏
    fixedSidebar: true,
    // 是否显示左侧栏顶部的logo
    sidebarLogo: true,
    // 是否开启水印
    isWater: false,
    // 是否开启 tabsView
    tabsView: true,
    // tabsView标签显示图标
    tabsViewIcon: false,
    // 面包屑第一层是否显示首页，否则会以当前点击的菜单根目录显示在第一层
    rootHome: true
  },

  /**
   * 是否显示动态标题
   */
  showDynamicTitle: true
}
