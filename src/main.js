import { createApp } from 'vue'

import App from './App.vue'
import router from './router'
import store from './store'

// view-ui-plus
import ViewUIPlus from 'view-ui-plus'
import 'view-ui-plus/dist/styles/viewuiplus.css'

// import global index.less
import '@/styles/index.less'

//引入vxe-table
import VXETable from 'vxe-table'
import 'vxe-table/lib/style.css'

import 'virtual:svg-icons-register' // 引入svg icon注册脚本

import '@/styles/vxe-table.scss'

import './permission'

// 引入全局指令
import * as directives from '@/directives/index.js'

//注册全局组件
import SvgIcon from '@/components/SvgIcon/index.vue'
import ColorIcon from '@/components/ColorIcon/index.vue'
import ILink from '@/components/ILink/index.vue'

function useTable(app) {
  app.use(VXETable)
}

const app = createApp(App)

app.use(router)
app.use(store)
app.use(ViewUIPlus)
app.use(useTable)
app.component('SvgIcon', SvgIcon)
app.component('ColorIcon', ColorIcon)
app.component('ILink', ILink)
app.mount('#app')
