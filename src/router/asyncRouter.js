export const asyncRouter = [
  {
    path: '/workSpace',
    name: 'workSpace',
    component: 'Layout',
    redirect: 'noRedirect',
    meta: {
      title: '工作空间',
      icon: 'md-home'
    },
    children: [
      {
        path: '/workSpace/control',
        name: 'workSpace-control',
        component: 'workSpace/control/index',
        meta: {
          title: '主控台',
          affix: true,
          icon: 'md-speedometer'
        }
      },
      {
        path: '/workSpace/analyse',
        name: 'workSpace-analyse',
        component: 'workSpace/analyse/index',
        meta: {
          title: '分析页',
          icon: 'ios-podium'
        }
      },
      {
        path: '/workSpace/workbench',
        name: 'workSpace-workbench',
        component: 'workSpace/workbench/index',
        meta: {
          title: '工作台',
          icon: 'ios-school'
        }
      }
    ]
  }
]
